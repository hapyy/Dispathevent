package com.haodaibao.coorditorlayout;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private Toolbar myToolBar;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private RecyclerView mRcv;
    private Myadapter adapter;
    private RelativeLayout mr;
    private List<String> mList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();
        initRcv();
        mRcv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                View view = linearLayoutManager.findViewByPosition(2);
//
//                if (view.getTop() <= 50) {
//                    mr.setVisibility(View.VISIBLE);
//                } else {
//                    mr.setVisibility(View.GONE);
//                }
            }
        });
    }

    private void initData() {
        for (int i = 0; i < 20; i++) {
            mList.add("item===" + i);
        }
    }

    private void initRcv() {
        linearLayoutManager = new LinearLayoutManager(this);
        mRcv.setLayoutManager(linearLayoutManager);
        adapter = new Myadapter(this, mList);
        mRcv.setAdapter(adapter);
    }

    private void initView() {
        myToolBar = (Toolbar) findViewById(R.id.mytoorbar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.mycollapsingtoolbarlayout);
        mRcv = (RecyclerView) findViewById(R.id.mrcv);
        mr = (RelativeLayout) findViewById(R.id.rl);
//        setSupportActionBar(myToolBar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mCollapsingToolbarLayout.setTitle("ssss");
        mCollapsingToolbarLayout.setTitleEnabled(false);
        mCollapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.red));
        mCollapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.green));
        myToolBar.setNavigationIcon(R.mipmap.ic_launcher);
        myToolBar.inflateMenu(R.menu.toolbarmenu);
//        myToolBar.setTitle("hphphp");

        myToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "navtigation", Toast.LENGTH_LONG).show();
            }
        });
        myToolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.search:
                        Toast.makeText(MainActivity.this, "search", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.delete:
                        Toast.makeText(MainActivity.this, "delete", Toast.LENGTH_LONG).show();
                        break;

                }
                return true;
            }
        });
    }
}
