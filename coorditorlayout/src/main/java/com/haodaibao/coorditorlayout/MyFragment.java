package com.haodaibao.coorditorlayout;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hdb on 2017/3/20.
 */

public class MyFragment extends Fragment {
    private Context mContext;
    private RecyclerView mRcv;
    private List<String> list = new ArrayList<>();

    public MyFragment(Context mContext) {
        this.mContext = mContext;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return LayoutInflater.from(mContext).inflate(R.layout.fragment_my, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRcv = (RecyclerView) view.findViewById(R.id.mrcv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mRcv.setLayoutManager(linearLayoutManager);
        initData();
    }

    private void initData() {
        list.clear();
        for (int i = 0; i < 40; i++) {
            list.add("index===" + i);
        }
        mRcv.setAdapter(new Myadapter(mContext, list));
    }
}
