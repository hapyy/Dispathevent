package com.haodaibao.coorditorlayout;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {
    private ViewPager mVp;
    private RelativeLayout mR;
    private TabLayout mtablayout;
    private MyPagerAdapter adapter;
    public List<MyFragment> list;
    public List<String> title;
    public AppBarLayout mAppbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coordinatortoolbar);
        initView();
        initData();
        adapter = new MyPagerAdapter(getSupportFragmentManager(), list, title);
        mVp.setAdapter(adapter);
        mtablayout.setupWithViewPager(mVp);
    }

    private void initData() {
        list = new ArrayList<>();
        title = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            list.add(new MyFragment(this));
            title.add("titile=="+i);
        }

    }

    private void initView() {
        mVp = (ViewPager) findViewById(R.id.viewpager);
        mR = (RelativeLayout) findViewById(R.id.mRelativelayout);
        mtablayout = (TabLayout)findViewById(R.id.mtablayout);
        mAppbar = (AppBarLayout) findViewById(R.id.appbar);
    }

    class MyPagerAdapter extends FragmentPagerAdapter {
        List<MyFragment> list;//ViewPager要填充的fragment列表
        List<String> title;//tab中的title文字列表

        //使用构造方法来将数据传进去
        public MyPagerAdapter(FragmentManager fm, List<MyFragment> list, List<String> title) {
            super(fm);
            this.list = list;
            this.title = title;
        }

        @Override
        public MyFragment getItem(int position) {//获得position中的fragment来填充
            return list.get(position);
        }

        @Override
        public int getCount() {//返回FragmentPager的个数
            return list.size();
        }

        //FragmentPager的标题,如果重写这个方法就显示不出tab的标题内容
        @Override
        public CharSequence getPageTitle(int position) {
            return title.get(position);
        }
    }
}
