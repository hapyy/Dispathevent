package com.haodaibao.coorditorlayout;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by hdb on 2017/2/13.
 */

public class Myadapter extends RecyclerView.Adapter<Myadapter.MyHolder> {
    private Context mContext;
    private List<String> mList;
    public Myadapter(Context context,List<String> mList) {
        this.mContext=context;
        this.mList = mList;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("hpo", "onCreateViewHolder: ");
        View mView = LayoutInflater.from(mContext).inflate(R.layout.item_recyclerview,parent,false);
        return new MyHolder(mView);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        Log.i("hpo", "onBindViewHolder: ");
            holder.mtv.setText(mList.get(position));
    }

    @Override
    public int getItemCount() {
        Log.i("hpo", "getItemCount: ");
        return mList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView mtv;

        public MyHolder(View itemView) {
            super(itemView);
            mtv = (TextView) itemView.findViewById(R.id.content);
        }
    }
}
