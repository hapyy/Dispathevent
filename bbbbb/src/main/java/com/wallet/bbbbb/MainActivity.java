package com.wallet.bbbbb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static{
        System.loadLibrary("demo");
    }
    private native String getStrFromJNI();
    private native  String getHpStrFromJNI();
    private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView) findViewById(R.id.tv);
        String ss = getStrFromJNI();
        String sw = getHpStrFromJNI();
//        String s = "012345678901234567890";
//        s=AddSpace(s);
        tv.setText(sw);
    }
    public static String AddSpace(String s){
        int length = s.length();
        int flag=0;
        String result="";
        for(int i=0;i<length-4;i=i+4){
            result=result+s.substring(i,i+4)+"\t";
            flag=i+4;
        }
        result=result+s.substring(flag);
        return result;
    }
}
