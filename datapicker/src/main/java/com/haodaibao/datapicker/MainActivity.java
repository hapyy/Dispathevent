package com.haodaibao.datapicker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements Dateshow {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onclick(View view) {
        switch(view.getId()){
        case R.id.ok:
            new DatePickFragment().show(getFragmentManager(),"hp");
        break;
        }
    }

    @Override
    public void show(int year, int month, int day) {
        Toast.makeText(this,year+"==="+month+"==="+day,Toast.LENGTH_LONG).show();
    }
}
