package com.wallet.demo1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static{
        System.loadLibrary("demo");
    }
    private native String getStrFromJNI();
    private native  String getHpStrFromJNI();
    private TextView mTv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTv = (TextView) findViewById(R.id.mtv);
        mTv.setText(getHpStrFromJNI());
    }
}
