package com.wallet.hhhhhhhh;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText et;
    public MyFragment mf;
    public FragmentManager fm;
    public Object o;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setEt((EditText) findViewById(R.id.et));
        getEt().setFocusableInTouchMode(true);
        fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.content,mf).commit();
    }
    public void function(){

    }

    public EditText getEt() {
        return et;
    }

    public void setEt(EditText et) {
        this.et = et;
    }
}
