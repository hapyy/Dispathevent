package com.wallet.mysdk;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.logging.Logger;

/**
 * Created by hdb on 2016/12/24.
 */

public abstract class BaseActivity extends AppCompatActivity {
    static{
        System.loadLibrary("demo");
    }
    private native String getStrFromJNI();
    private native  String getHpStrFromJNI();
    private TextView mTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activitybase);
        mTv = (TextView) findViewById(R.id.mtv);
        mTv.setText(getHpStrFromJNI());
    }


}
