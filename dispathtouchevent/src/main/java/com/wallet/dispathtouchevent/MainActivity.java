package com.wallet.dispathtouchevent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView tv_1;
    private TextView tv_2;
    private TextView tv_3;
    private TextView tv_4;
    private MyLayout mylayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        tv_1.setOnClickListener(l);
        tv_2.setOnClickListener(l);
        tv_3.setOnClickListener(l);
        tv_4.setOnClickListener(l);
//        mylayout.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Log.i("hpo", "onTouch: mylayout"+v.getId()+"action"+event.getAction());
//                return false;
//            }
//        });
        //git 测试
        //git newbrach分支测试
        int coorda=90;
        mylayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("hpo", "onClick: mylayout");
            }
        });
    }

    private void initView() {
        tv_1 = (TextView) findViewById(R.id.tv_1);
        tv_2 = (TextView) findViewById(R.id.tv_2);
        tv_3 = (TextView) findViewById(R.id.tv_3);
        tv_4 = (TextView) findViewById(R.id.tv_4);
        mylayout = (MyLayout) findViewById(R.id.mylayout);
    }

    public OnClickListener l = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i("hpo", "onClick: " + v.getId());
        }
    };
}
