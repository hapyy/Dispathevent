package com.wallet.dispathtouchevent;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * Created by hdb on 2017/1/10.
 */

public class MyLayout extends LinearLayout {
    public MyLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.i("hpo", "onTouchEvent: mylayout====ontouchevent()");
        Log.i("hpo", "onTouchEvent: "+super.onTouchEvent(event));
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        //return super.onInterceptTouchEvent(ev);
        return false;
    }
}
